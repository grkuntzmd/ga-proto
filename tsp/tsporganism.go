/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package tsp

import (
	"fmt"
	"log"
	"math"
	"math/rand"
	"strconv"
	"strings"

	"bitbucket.org/grkuntzmd/ga-proto/engine"
)

const (
	crossoverCutLength = 20
)

// Organism represents an organism for the traveling salesperson problem.
type Organism struct {
	cities []*City
	score  float64
}

// InitializeEngine initializes the genetic algorithm engine with random organisms for the zeroeth generation.
func InitializeEngine(g *engine.Genetic, generationSize int) {
	var organisms []engine.Organism
	numCities := len(*cities)

	for o := 0; o < generationSize; o++ {
		p := g.Perm(numCities)
		var c []*City

		for i := 0; i < len(p); i++ {
			c = append(c, (*cities)[p[i]])
		}

		organisms = append(organisms, &Organism{cities: c})
	}

	g.Configure(engine.InitialGeneration(organisms))
}

// Clone returns a copy of the current organism.
func (t *Organism) Clone() engine.Organism {
	o := Organism{cities: make([]*City, len(t.cities)), score: t.score}
	copy(o.cities, t.cities)
	return &o
}

// Cross crosses this organism with another.
func (t *Organism) Cross(prob float64, other engine.Organism, r *rand.Rand) {
	if prob >= r.Float64() {
		o := other.(*Organism)

		cityCount := len(t.cities)

		firstCutPoint := r.Intn(cityCount - crossoverCutLength)
		secondCutPoint := firstCutPoint + crossoverCutLength

		offspring1 := make([]*City, cityCount)
		offspring2 := make([]*City, cityCount)

		used1 := make(map[*City]bool)
		used2 := make(map[*City]bool)

		// Handle the middle sections.
		for i := firstCutPoint; i < secondCutPoint; i++ {
			offspring1[i] = o.cities[i]
			offspring2[i] = t.cities[i]
			used1[offspring1[i]] = true
			used2[offspring2[i]] = true
		}

		// Handle the outer sections.
		for i := 0; i < firstCutPoint; i++ {
			offspring1[i] = findUnused(used1, t.cities)
			offspring2[i] = findUnused(used2, o.cities)
		}
		for i := secondCutPoint; i < cityCount; i++ {
			offspring1[i] = findUnused(used1, t.cities)
			offspring2[i] = findUnused(used2, o.cities)
		}

		copy(t.cities, offspring1)
		copy(o.cities, offspring2)
	}
}

func findUnused(used map[*City]bool, cities []*City) *City {
	for _, c := range cities {
		if !used[c] {
			used[c] = true
			return c
		}
	}

	return nil // Should never happen.
}

// Mutate mutates the organism with the given probability.
func (t *Organism) Mutate(prob float64, r *rand.Rand) {
	l := len(t.cities)
	for i := 0; i < l; i++ {
		if prob >= r.Float64() {
			other := r.Intn(l)
			for ; other == i; other = r.Intn(l) {
			}
			t.cities[i], t.cities[other] = t.cities[other], t.cities[i]
		}
	}
}

// SetScore calculates the score for this organism.
func (t *Organism) SetScore() {
	lastLon := t.cities[0].Lon
	lastLat := t.cities[0].Lat
	var score float64
	for i := 1; i < len(t.cities); i++ {
		curLon := t.cities[i].Lon
		curLat := t.cities[i].Lat
		score += haversine(curLon, curLat, lastLon, lastLat)
		lastLon = curLon
		lastLat = curLat
	}

	t.score = score
}

// Score retrieves the score for this organism.
func (t *Organism) Score() float64 {
	return t.score
}

func (t *Organism) String() string {
	var cities []string

	for _, c := range t.cities {
		cities = append(cities, c.Name)
	}

	return fmt.Sprintf("%f: %s", t.score, strings.Join(cities, " -> "))
}

// City represents a single city with its location.
type City struct {
	Name  string  // City name
	State string  // State name
	Lat   float64 // Latitude
	Lon   float64 // Longitude
}

var cities *[]*City

// NewCities initializes the cities array from am array of string arrays.
func NewCities(records [][]string) {
	cities = new([]*City)

	for _, row := range records {
		lat, err := strconv.ParseFloat(row[2], 64)
		if err != nil {
			log.Panicf("error parsing %s: %s", row[2], err)
		}

		lon, err := strconv.ParseFloat(row[3], 64)
		if err != nil {
			log.Panicf("error parsing %s: %s", row[3], err)
		}

		city := &City{Name: row[1], State: row[0], Lat: lat, Lon: lon}
		*cities = append(*cities, city)
	}
}

func (c *City) String() string {
	return c.Name
}

const earthRadius = float64(6371)

// haversine calculates the great-circle distance between two locations using the haversine formula (https://en.wikipedia.org/wiki/Haversine_formula).
// From https://play.golang.org/p/MZVh5bRWqN.
func haversine(lonFrom float64, latFrom float64, lonTo float64, latTo float64) (distance float64) {
	var deltaLat = (latTo - latFrom) * (math.Pi / 180)
	var deltaLon = (lonTo - lonFrom) * (math.Pi / 180)

	var a = math.Sin(deltaLat/2)*math.Sin(deltaLat/2) + math.Cos(latFrom*(math.Pi/180))*math.Cos(latTo*(math.Pi/180))*math.Sin(deltaLon/2)*math.Sin(deltaLon/2)
	var c = 2 * math.Atan2(math.Sqrt(a), math.Sqrt(1-a))

	distance = earthRadius * c

	return
}
