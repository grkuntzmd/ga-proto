/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package engine

import (
	"fmt"
	"math"
	"math/rand"
	"runtime"
	"sort"
	"sync"
	"time"
)

const (
	elitistFraction      = float64(1) // Fill in any leftovers with elitists.
	mutationProbability  = float64(0.05)
	crossoverProbability = float64(0.1)
	stopGenerations      = 50
	tournamentFraction   = float64(0.99)
)

// Genetic implements the genetic algorithm engine.
type Genetic struct {
	// System configuration options.
	parallel             bool    // Spawn enough goroutines to process all of the individuals in a generation instead of using workers.
	verbose              bool    // Show verbose output during processing.
	stopGenerations      int     // The number of generation that must pass with no improvement for the engine to stop.
	mutationProbability  float64 // The probability that an organim mutates when being copied.
	crossoverProbability float64 // The probability that an organim crosses with another when being copied.
	elitistFraction      float64 // The proportion of best individuals that are copied to the next generation.
	tournamentFraction   float64 // The proportion of the generation size that undergoes tournament selection for mating.

	// User configuration options.
	currentGeneration *[]Organism // The current generation of organisms.

	// Working fields.
	nextGeneration   *[]Organism // The next generation built from the current generation -- will replace the current generation after it is built.
	generationNumber int         // The current generation number.
	bestScore        float64     // The best score of any generation seen so far.
	noImprovement    int         // Number of generations since the last one with an improved best average score.
	generationSize   int         // Number of individuals in each gneration.
	*rand.Rand                   // The random number generator to use for all operations.
}

// Organism defines that methods that an organism must implement.
type Organism interface {
	Clone() Organism                     // Clone an organism and return a new copy.
	Cross(float64, Organism, *rand.Rand) // Cross this organism with another.
	Mutate(float64, *rand.Rand)          // Mutate an organism.
	SetScore()                           // Calculate the score of this organism.
	Score() float64                      // Return the score for this organism.
}

// New returns a new genetic algorithm engine pre-configured with the given system options.
func New(options ...func(*Genetic)) *Genetic {
	g := &Genetic{
		crossoverProbability: crossoverProbability,
		mutationProbability:  mutationProbability,
		stopGenerations:      stopGenerations,
		elitistFraction:      elitistFraction,
		tournamentFraction:   tournamentFraction,
		bestScore:            math.MaxFloat64,
		Rand:                 rand.New(rand.NewSource(time.Now().Unix())),
	}

	for _, option := range options {
		option(g)
	}

	return g
}

type userGenetic *Genetic

// Configure sets the user configuration options on the genetic algorithm engine.
func (g *Genetic) Configure(options ...func(userGenetic)) {
	for _, option := range options {
		option(userGenetic(g))
	}
}

// Run executes the engine until the stop conditions are met then returns the best individual.
func (g *Genetic) Run() Organism {
	start := time.Now()

	for {
		g.runGeneration()
		bestScore := (*g.currentGeneration)[0].Score()

		if g.verbose {
			fmt.Printf("generation %d, best score: %f\n", g.generationNumber, bestScore)
		}

		if bestScore < g.bestScore {
			g.bestScore = bestScore
			g.noImprovement = 0
		} else {
			g.noImprovement++
			if g.noImprovement >= g.stopGenerations {
				break
			}
		}

		nextGeneration := make([]Organism, 0, g.generationSize)
		g.nextGeneration = &nextGeneration

		g.tournament()
		g.doCrossovers()
		g.doMutations()
		g.elitist() // Copy some of best from the previous generation so we do not lose those.

		g.currentGeneration = g.nextGeneration
	}

	if g.verbose {
		fmt.Printf("stopped after %d generations with no improvement, time: %s\n", g.generationNumber, time.Since(start))
	}

	return (*g.currentGeneration)[0]
}

// available returns the number of organisms that can still be added to the next generation.
func (g *Genetic) available(requested int) int {
	max := g.generationSize - len(*g.nextGeneration)
	if requested <= max {
		return requested
	}

	return max
}

// doCrossovers performs crossovers on the next generation.
func (g *Genetic) doCrossovers() {
	for i, o := range *g.nextGeneration {
		other := g.Intn(len(*g.nextGeneration))
		for ; other == i; other = g.Intn(len(*g.nextGeneration)) {
		}
		o.Cross(g.crossoverProbability, (*g.nextGeneration)[other], g.Rand)
	}
}

// doMutations performs mutations on the next generation.
func (g *Genetic) doMutations() {
	for _, o := range *g.nextGeneration {
		o.Mutate(g.mutationProbability, g.Rand)
	}
}

// elitist copies the elitist fraction of the best organisms to the next generation.
func (g *Genetic) elitist() {
	count := g.available(int(g.elitistFraction * float64(g.generationSize)))
	for i := 0; i < count; i++ {
		organism := (*g.currentGeneration)[i].Clone()
		*g.nextGeneration = append(*g.nextGeneration, organism)
	}
}

// runGeneration scores and sorts the current generation.
func (g *Genetic) runGeneration() {
	if g.parallel {
		g.setScoresParallel()
	} else {
		g.setScoresWorkers()
	}

	sort.Slice(*g.currentGeneration, func(i, j int) bool {
		return (*g.currentGeneration)[i].Score() < (*g.currentGeneration)[j].Score()
	})

	g.generationNumber++
}

// setScoresParallel sets the scores for all of the organisms in the current generation in parallel using goroutines.
func (g *Genetic) setScoresParallel() {
	var wg sync.WaitGroup

	for _, o := range *g.currentGeneration {
		wg.Add(1)
		go func(o Organism) {
			defer wg.Done()
			o.SetScore()
		}(o)
	}

	wg.Wait()
}

// setScoresWorkers sets the scores for all of the organisms in the current generation using a number of worker equal to the number of CPUs.
func (g *Genetic) setScoresWorkers() {
	numWorkers := runtime.NumCPU() * 2

	var wg sync.WaitGroup
	wg.Add(numWorkers)

	work := make(chan Organism, g.generationSize)

	for w := 0; w < numWorkers; w++ {
		go func() {
			defer wg.Done()

			for o := range work {
				o.SetScore()
			}
		}()
	}

	for _, o := range *g.currentGeneration {
		work <- o
	}
	close(work)

	wg.Wait()
}

// tournament copies organisms from the current generation to the next generation using a tournament. Two random organisms are selected; the one with the best score is copied.
func (g *Genetic) tournament() {
	count := g.available(int(g.tournamentFraction * float64(g.generationSize)))
	for c := 0; c < count; c++ {
		f := (*g.currentGeneration)[g.Intn(g.generationSize)]
		s := (*g.currentGeneration)[g.Intn(g.generationSize)]
		if f.Score() <= s.Score() {
			*g.nextGeneration = append(*g.nextGeneration, f.Clone())
		} else {
			*g.nextGeneration = append(*g.nextGeneration, s.Clone())
		}
	}
}
