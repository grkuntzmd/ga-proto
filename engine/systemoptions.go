/*
 * MIT LICENSE
 *
 * Copyright © 2017, G.Ralph Kuntz, MD.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package engine

import "math/rand"

// CrossoverProbability sets the probability of crossover.
func CrossoverProbability(p float64) func(*Genetic) {
	return func(g *Genetic) {
		g.crossoverProbability = p
	}
}

// ElitistFraction sets the elitist fraction.
func ElitistFraction(v float64) func(*Genetic) {
	return func(g *Genetic) {
		g.elitistFraction = v
	}
}

// MutationProbability sets the probability of mutation.
func MutationProbability(p float64) func(*Genetic) {
	return func(g *Genetic) {
		g.mutationProbability = p
	}
}

// RandomGenerator sets the random number generator.
func RandomGenerator(r *rand.Rand) func(*Genetic) {
	return func(g *Genetic) {
		g.Rand = r
	}
}

// StopGenerations sets the stop generation count.
func StopGenerations(v int) func(*Genetic) {
	return func(g *Genetic) {
		g.stopGenerations = v
	}
}

// TournamentFraction sets the tournament fraction.
func TournamentFraction(v float64) func(*Genetic) {
	return func(g *Genetic) {
		g.tournamentFraction = v
	}
}

func Parallel(g *Genetic) {
	g.parallel = true
}

// Verbose sets the verbose flag.
func Verbose(g *Genetic) {
	g.verbose = true
}
